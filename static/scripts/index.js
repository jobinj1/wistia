// app module
var appModule = angular.module("appModule", ["ngRoute"]);

// routing configuration
appModule.config(function ($routeProvider) {
    $routeProvider.when("/", {
        template: "<home></home>"
    });
});