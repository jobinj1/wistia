// home component
appModule.component("home", {
    templateUrl: "../pages/home.html",
    controller: homeController
});

// home controller
function homeController($scope, $element, $attrs, $http) {

    // during initialization
    this.$onInit = function () {

        // toast message options
        toastr.options = {
            "positionClass": "toast-bottom-center"
        }

        // get media list
        getMediaList($scope, $http);

    };

    // handle file upload
    var checkProgress;

    $(function () {

        $("#uploader").fileupload({

            dataType: "json",

            progress: function (e, data) {
                // update progress bar
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $("#percent").text("Uploading to server (step 1 of 2): " + progress + "%");
                $("#progress .bar").css("width", progress + "%");
                // check progress of upload to Wistia
                if (progress >= 100) {
                    checkProgress = setInterval(function () {
                        getUploadProgress($http);
                    }, 1000);
                }
            },

            done: function (e, data) {
                // refresh media list
                getMediaList($scope, $http);
            },

            fail: function (e, data) {
                // display the error
                toastr.error(data.jqXHR.responseText);
            },

            always: function (e, data) {
                // remove progress bar
                $("#percent").text("");
                $("#progress .bar").css("width", "0%");
                clearInterval(checkProgress);
            }

        });

    });

    // when user requests deletion of media
    $scope.removeMedia = function (hashed_id) {

        // prepare the request
        var req = {
            method: "DELETE",
            url: "/deleteMedia",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                hashed_id: hashed_id
            }
        }

        // send http request
        $http(req).then(
            function (res) {
                // refresh media list
                getMediaList($scope, $http);
            },
            function (res) {
                // display the error
                toastr.error(res.message);
            }
        );

    }

    /* when user uploads a media file
    $scope.addMedia = function (fileInfo) {

        // file information
        var formData = new FormData();
        formData.append("media", fileInfo);

        // prepare the request
        var req = {
            method: "POST",
            url: "/uploadMedia",
            headers: {
                "Content-Type": undefined
            },
            data: formData
        }

        // send http request
        $http(req).then(
            function (res) {
                // refresh media list
                getMediaList($scope, $http);
            },
            function (res) {
                // display the error
                toastr.error(res.message);
            }
        );

    } */

}


// get the list of media files
var getMediaList = function ($scope, $http) {

    // prepare the request
    var req = {
        method: "GET",
        url: "/listMedia",
        headers: {
            "Content-Type": "application/json"
        }
    }

    // send http request
    $http(req).then(
        function (res) {
            // save the list into current scope
            $scope.mediaList = [];
            for (var i = 0; i < res.data.length; i++) {
                $scope.mediaList.push({
                    name: res.data[i].name,
                    type: res.data[i].type,
                    hashed_id: res.data[i].hashed_id,
                    ng_class: "wistia_async_" + res.data[i].hashed_id,
                    width: res.data[i].assets[0].width,
                    height: res.data[i].assets[0].height
                });
            }
        },
        function (res) {
            // display the error
            toastr.error(res.message);
        }
    );

}

// get progress of upload to Wistia
var getUploadProgress = function ($http) {

    // prepare the request
    var req = {
        method: "GET",
        url: "/uploadStatus",
        headers: {
            "Content-Type": "application/json"
        }
    }

    // send http request
    $http(req).then(
        function (res) {
            $("#percent").text("Uploading to Wistia (step 2 of 2): " + res.data.progress + "%");
            $("#progress .bar").css("width", res.data.progress + "%");
        },
        function (res) {
            // display the error
            toastr.error(res.message);
        }
    );

}