/* data declarations */

const
  express = require("express"),
  bodyParser = require("body-parser"),
  multer = require("multer"),
  fs = require("fs"),
  path = require("path"),
  contentTypes = require("./utils/content-types");


/* for file upload */

//multer disk storage settings
let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./attachments/")
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
});

//multer settings
let upload = multer({
  storage: storage
}).single("media");


/* server initialization */

// server components
let server = express();
let http = require("http").createServer(server);

// listen on port
http.listen(process.env.NODE_PORT || 3000, process.env.NODE_IP || "localhost", function () {
  console.log(`Application worker ${process.pid} started...`);
});

// report server health
server.get("/health", function (req, res) {
  res.writeHead(200);
  res.end();
});

// serve static objects
server.use(express.static("node_modules"));

server.use(express.static("static"), function (req, res, next) {
  let ext = path.extname(req.path).slice(1);
  if (contentTypes[ext]) {
    res.setHeader("Content-Type", contentTypes[ext]);
  }
  if (ext === "html") {
    res.setHeader("Cache-Control", "no-cache, no-store");
  }
  next();
});

// to parse request body
server.use(bodyParser.json());


/* server requests */

const
  wistia = require('./submodules/wistia');

wistia(server, upload, fs);