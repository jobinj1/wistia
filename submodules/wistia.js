const
    httpRequest = require("request"),

    apiPassword = "api_password=ecd1a481e1897ffd6f1dd1491d94cbbfb4ab8fbef16cfd6501c196d2f45cbe72",
    projectId = "project_id=2985166";

let
    uploadRequest,
    uploadSize;


module.exports = function (server, upload, fs) {

    /* get the list of media files */
    server.get("/listMedia", function (req, res) {

        let wistiaUrl = "https://api.wistia.com/v1/medias.json?" + projectId + "&" + apiPassword;

        httpRequest(wistiaUrl, function (err, response, body) {
            if (err) {
                res.status(500).send({
                    message: err
                });
            } else {
                res.send(JSON.parse(body));
            }
        });

    });


    /* delete media file */
    server.delete("/deleteMedia", function (req, res) {

        let options = {
            method: "DELETE",
            url: "https://api.wistia.com/v1/medias/" + req.body.hashed_id + ".json?" + apiPassword
        };

        httpRequest(options, function (err, response, body) {
            if (err) {
                res.status(500).send({
                    message: err
                });
            } else {
                res.send(JSON.parse(body));
            }
        });

    });


    /* upload media file */
    server.post("/uploadMedia", function (req, res) {

        upload(req, res, function (err) {

            if (err) {

                res.status(500).send({
                    message: err
                });

            } else {

                // save file size
                uploadSize = req.file.size;

                let options = {
                    method: "POST",
                    url: "https://upload.wistia.com?" + projectId + "&" + apiPassword,
                    formData: {
                        file: fs.createReadStream(req.file.path)
                    }
                };

                // save reference of the request object
                uploadRequest = httpRequest(options, function (err, response, body) {
                    fs.unlink(req.file.path, function (err) { });
                    if (err) {
                        res.status(500).send({
                            message: err
                        });
                    } else if (JSON.parse(body).error) {
                        res.status(500).send({
                            message: JSON.parse(body).error
                        });
                    } else {
                        res.send(JSON.parse(body));
                    }
                });

            }

        });

    });


    /* get upload status */
    server.get("/uploadStatus", function (req, res) {

        let progress = 0;

        if (uploadSize == 0 || uploadRequest == null) {

            res.status(500).send({
                message: "No upload in progress"
            });

        } else {

            progress = Math.floor((uploadRequest.req.connection._bytesDispatched / uploadSize) * 100);
            if (progress > 100) {
                progress = 100;
            }

            res.send({ progress: progress });

        }

    });

}